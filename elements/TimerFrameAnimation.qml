import QtQuick //2.3
import "../"

Item {
    id: root

    //property bool useFrameAnimation: true
    property bool running: false
    property alias frameTime: frameAnim.frameTime

    signal triggered()

    FrameAnimation {
        id: frameAnim
        running: root.running && mainRoot.useFrameAnimation
        //interval: fps_ms
        //repeat: true
        onTriggered: {
            console.log("FrameAnimation frameTime: ", frameTime)
            fps = 60
            if (useFrameAnimation)
                root.triggered()
        }

    }

    Timer {
        running: root.running && !mainRoot.useFrameAnimation
        interval: fps_ms
        repeat: true
        onTriggered: {
            if (!useFrameAnimation)
                root.triggered()
        }
    }
}
